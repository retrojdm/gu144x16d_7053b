////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// text-scroll.ino
//
// Andrew Wyatt
// retrojdm.com
//
// This is a sketch to demonstrate the .scroll() function.

// If you want to use a hardware serial instead, you don't need this.
#include <SoftwareSerial.h>

#include "gu7000.h"
#include "hal/gu7000UartHal.h"

// The pins below are configured for an ATMEGA1284 in the "Standard" pinout.
#define VFD_BAUD_RATE 115200 // Default is 38400. See VFD Jumpers for more options.
#define PIN_VFD_SERIAL_RX 10  // Not actually used
#define PIN_VFD_SERIAL_TX 11  // This goes to the SIN pin on the VFD
#define PIN_VFD_SBUSY 12
#define PIN_VFD_RESET 13

// These values are for a GU112X16G-7003. You might need to change them for your own display.
#define VFD_DISPLAY_WIDTH 112 // px
#define VFD_DISPLAY_HEIGHT 16 // px
#define VFD_MEMORY_WIDTH 512  // px
#define VFD_MEMORY_HEIGHT 16  // px

#define SCROLL_DELAY 25 // ms

// Only needed if we're not using a hardware serial. Eg: Serial or Serial1 etc.
SoftwareSerial vfdSerial(PIN_VFD_SERIAL_RX, PIN_VFD_SERIAL_TX);

// An instance of the gu7000UartHal class.
// This is a Hardware Abstraction layer for asynchronous serial (UART) mode.
// See gu7000I2cHal or write a custom HAL for other modes if your display supports them.
gu7000UartHal vfdHal(
    &vfdSerial,
    PIN_VFD_SBUSY,
    PIN_VFD_RESET);

// Error code "2" means we timed out waiting over 1 second for the SBusy pin to go LOW.
// This could occur if we didn't reset properly or if the read buffer is full.
static void errorCallback(uint8_t code)
{
    Serial.begin(9600);
    Serial.print("Error ");
    Serial.println(code);
    while (true)
        ;
}

// An instance of the gu7000 class.
gu7000 vfd(
    VFD_DISPLAY_WIDTH,
    VFD_DISPLAY_HEIGHT,
    VFD_MEMORY_WIDTH,
    VFD_MEMORY_HEIGHT,
    &vfdHal,
    errorCallback);

/*
You can generate custom characters using either of the following tools:

Remember, custom characters must be either 5x7 or 7x8 px

    http://www.noritake-elec.com/codeLibraries/gu7000/gu7000_BitmapImageTool.html
    http://en.radzio.dxp.pl/bitmap_converter/
*/
/* 7x8 image "Heart" */
static const uint8_t image_Heart[] = {
    0x78, 0x84, 0x82, 0x41, 0x82, 0x84, 0x78};

void setup()
{
    // We need to begin the Serial before calling vfd.init();
    pinMode(PIN_VFD_SERIAL_RX, INPUT);
    pinMode(PIN_VFD_SERIAL_TX, OUTPUT);
    vfdSerial.begin(VFD_BAUD_RATE);

    vfd.reset();
    vfd.init();

    vfd.brightness(4); // 1..8

    // "Base" window.
    vfd.windowSelect(0);

    // Allow drawing to all the display memory (not just the visible part).
    vfd.writeScreenMode(1);

    // Clears the current window.
    vfd.cls();

    // x can be 1 .. 4
    // y can be 1 .. 2
    vfd.fontMagnification(1, 2);

    // 0 = Fixed character width 1 (1 dot space on right side)
    // 1 = Fixed character width 2 (1 dot space on right side and left side)
    // 2 = Proportional character width 1 (1 dot space on right side)
    // 3 = Proportional character width 2 (1 dot space on right side and left side)
    vfd.fontWidthMode(3); // We need to use a "width 3" fontWidth to show the full 7 pixels of my custom heart character.

    // We're turning custom characters on, and downloading a heart symbol to replace the 'H' character.
    // if we tunr custom characters off, the normal 'H' character would be drawn.
    vfd.showCustomChars(true);
    vfd.downloadCustomChars(7, 'H', 'H', (byte *)&image_Heart);

    // Write some text to display memory.
    char buffer[256];
    sprintf(
        buffer,
        "Memory is %dx%dpx wide. Display shows %dx%dpx. H",
        vfd.memoryWidth,
        vfd.memoryHeight,
        vfd.displayWidth,
        vfd.displayHeight);

    // You'll have to be careful not to render more than the window width (we're using the base window in "All screen
    // mode" in this example, so the limit should be the same as memory width. i.e.: 512px).
    vfd.print(buffer);

    // Wait half a second before scrolling, so we have a chance to see the start of the text.
    delay(500);
}

uint32_t lastScroll = 0;
void loop()
{
    long now = millis();
    if (now - lastScroll >= SCROLL_DELAY)
    {
        lastScroll = now;

        // The scroll function is more of a shift function.
        // We shift "width" = 2 bytes.
        // The display is drawn column by column, starting at the top left.
        // Since the display is 16 pixels high, and there's 8 bits per byte, that's 2 bytes per column.
        // We want to scroll left by one pixel, so we need to "shift" 2 bytes.
        // cycles and speed aren't needed, since we're doing this in a loop.
        vfd.scroll(2, 1, 0);
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// sprite-anim.ino
//
// Andrew Wyatt
// retrojdm.com
//
// This is a sketch to demonstrate blitting a single sprite on a Noritake Itron GU112X16G-7003B VFD at any position
// (even on the earlier revision "A" models with firmware F100).
//
// This sketch also demostrates how to implement a double-buffer for flicker-free animation. This is achieved by drawing
// to a "window" that's out of view, then shifting it into view each frame when ready.

// If you want to use a hardware serial instead, you don't need this.
#include <SoftwareSerial.h>

#include "gu7000.h"
#include "hal/gu7000UartHal.h"

// The pins below are configured for an ATMEGA1284 in the "Standard" pinout.
#define VFD_BAUD_RATE 115200 // Default is 38400. See VFD Jumpers for more options.
#define PIN_VFD_SERIAL_RX 10  // Not actually used
#define PIN_VFD_SERIAL_TX 11  // This goes to the SIN pin on the VFD
#define PIN_VFD_SBUSY 12
#define PIN_VFD_RESET 13

// These values are for a GU112X16G-7003. You might need to change them for your own display.
#define VFD_DISPLAY_WIDTH 112 // px
#define VFD_DISPLAY_HEIGHT 16 // px
#define VFD_MEMORY_WIDTH 512  // px
#define VFD_MEMORY_HEIGHT 16  // px

#define FRAME_RATE 30 // frames per second
#define FRAME_DELAY 1000 / FRAME_RATE
#define MAX_FRAMESKIP 10
#define SCROLL_BYTES VFD_DISPLAY_WIDTH *(VFD_DISPLAY_HEIGHT >> 3)

// Only needed if we're not using a hardware serial. Eg: Serial or Serial1 etc.
SoftwareSerial vfdSerial(PIN_VFD_SERIAL_RX, PIN_VFD_SERIAL_TX);

// An instance of the gu7000UartHal class.
// This is a Hardware Abstraction layer for asynchronous serial (UART) mode.
// See gu7000I2cHal or write a custom HAL for other modes if your display supports them.
gu7000UartHal vfdHal(
    &vfdSerial,
    PIN_VFD_SBUSY,
    PIN_VFD_RESET);

// Error code "2" means we timed out waiting over 1 second for the SBusy pin to go LOW.
// This could occur if we didn't reset properly or if the read buffer is full.
static void errorCallback(uint8_t code)
{
    Serial.begin(9600);
    Serial.print("Error ");
    Serial.println(code);
    while (true)
        ;
}

// An instance of the gu7000 class.
gu7000 vfd(
    VFD_DISPLAY_WIDTH,
    VFD_DISPLAY_HEIGHT,
    VFD_MEMORY_WIDTH,
    VFD_MEMORY_HEIGHT,
    &vfdHal,
    errorCallback);

// You can generate sprites using either of the following tools:
//
//  http://www.noritake-elec.com/codeLibraries/gu7000/gu7000_BitmapImageTool.html
//  http://en.radzio.dxp.pl/bitmap_converter/

// 32x32 image "test-sprite"
static const uint8_t sprData[] = {
    0x00, 0x07, 0xe0, 0x00, 0x00, 0x3f, 0xfc, 0x00, 0x00, 0xff, 0xff, 0x00, 0x01, 0xff, 0xff, 0x80,
    0x07, 0xf8, 0x1f, 0xe0, 0x0f, 0xe0, 0x07, 0xf0, 0x0f, 0x80, 0x01, 0xf0, 0x1f, 0x00, 0x00, 0xf8,
    0x3e, 0x00, 0x18, 0x7c, 0x3c, 0x00, 0x0c, 0x3c, 0x7c, 0x60, 0x0e, 0x3e, 0x78, 0xf0, 0x0e, 0x1e,
    0x78, 0x60, 0x0f, 0x1e, 0xf0, 0x00, 0x0f, 0x0f, 0xf0, 0x00, 0x0f, 0x0f, 0xf0, 0x00, 0x1f, 0x0f,
    0xf0, 0x60, 0x1f, 0x8f, 0xf0, 0xf0, 0x3f, 0x8f, 0xf0, 0x60, 0x3f, 0x8f, 0x78, 0x00, 0x7f, 0x1e,
    0x78, 0x00, 0x7f, 0x1e, 0x7c, 0x00, 0xff, 0x3e, 0x3c, 0x01, 0xfe, 0x3c, 0x3e, 0x07, 0xfc, 0x7c,
    0x1f, 0x0f, 0xf0, 0xf8, 0x0f, 0x80, 0x01, 0xf0, 0x0f, 0xe0, 0x07, 0xf0, 0x07, 0xf8, 0x1f, 0xe0,
    0x01, 0xff, 0xff, 0x80, 0x00, 0xff, 0xff, 0x00, 0x00, 0x3f, 0xfc, 0x00, 0x00, 0x07, 0xe0, 0x00};

static const struct sprite spr = {
    .width = 32,
    .height = 32,
    .xOffset = 16, // (x,y) handle
    .yOffset = 16,
    .data = sprData};

void setup()
{
    // We need to begin the Serial before calling vfd.init();
    pinMode(PIN_VFD_SERIAL_RX, INPUT);
    pinMode(PIN_VFD_SERIAL_TX, OUTPUT);
    vfdSerial.begin(VFD_BAUD_RATE);

    vfd.reset();
    vfd.init();

    vfd.brightness(4); // 1..8

    vfd.writeScreenMode(0);
    vfd.windowDefine(1, vfd.displayWidth, 0, vfd.displayWidth, vfd.displayHeight >> 3);
    vfd.windowSelect(1);
}

// Global state.
int16_t x = vfd.displayWidth / 2;
int16_t y = vfd.displayHeight / 2;
int16_t dx = 2;
int16_t dy = 1;
uint32_t nextTick = 0;

void loop()
{
    // On slower hardware (or when there's a lot to render), we skip frames.
    uint8_t loops = 0;
    while ((millis() > nextTick) && (loops < MAX_FRAMESKIP))
    {
        update();

        nextTick += FRAME_DELAY;
        loops++;
    }

    render();
}

void update()
{
    x += dx;
    if (x <= 0 || x >= vfd.displayWidth)
    {
        dx = -dx;
    }

    y += dy;
    if (y <= 0 || y >= vfd.displayHeight)
    {
        dy = -dy;
    }
}

void render()
{
    vfd.cls();
    vfd.blit(spr, x, y, false);
    vfd.scroll(SCROLL_BYTES, 1, 0);
}

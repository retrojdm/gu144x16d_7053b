////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// dog-simulator.ino
//
// Andrew Wyatt
// retrojdm.com
//
// This is a sketch to demonstrate blitting sprites on a Noritake Itron GU-7000 series VFD.
// Eg: 144X16D-7053B or 112X16G-7003
//
// All my dog "Sunny" ever seems to do is eat, bark and poop. I thought I'd make a little sketch to celebrate her
// talents.

// If you want to use a hardware serial instead, you don't need this.
#include <SoftwareSerial.h>

#include "gu7000.h"
#include "hal/gu7000UartHal.h"

// See this header file for pins etc.
#include "dog-simulator.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Functions

void setup()
{

    // Set the button pins as inputs, with a pullup resistor.
    // This means they're usually HIGH, and are pulled LOW when pressed.
    for (byte b = 0; b < BUTTONS; b++)
    {
        pinMode(button[b].pin, INPUT_PULLUP);
    }

    // We need to begin the Serial before calling vfd.init();
    pinMode(PIN_VFD_SERIAL_RX, INPUT);
    pinMode(PIN_VFD_SERIAL_TX, OUTPUT);
    vfdSerial.begin(VFD_BAUD_RATE);

    vfd.reset();
    vfd.init();

    vfd.brightness(4); // 1..8

    // We need to set up a window in the hidden display memory.
    // We'll draw to this hidden part, and when finished, we'll "shift" it into view.
    // This will stop the flickering that occours from the .cls() when drawing directly to the visible part of the
    // display.
    vfd.writeScreenMode(1); // Allow drawing to all the display memory (not just the visible part).
    vfd.windowDefine(1, vfd.displayWidth, 0, vfd.displayWidth, vfd.displayHeight / 8);
    vfd.windowSelect(1);

    // OR the pixels together, instead of overwrite. This effectively makes "0" pixels transparent.
    vfd.writeMixDisplayMode(1);

    // place the puppy and treats from the level data.
    initLevel();
}

void loop()
{
    // This is the main game loop.

    // On slower hardware (or when there's a lot to render), we skip frames.
    byte loops = 0;
    while ((millis() > nextTick) && (loops < MAX_FRAMESKIP))
    {

        readButtons();
        updateGame();

        nextTick += FRAME_DELAY;
        loops++;
    }

    render();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions

void changeState(objectType *obj, byte state)
{

    obj->state = state;
    obj->tick = 0;
    obj->frame = 0;

    switch (state)
    {

    case STATE_STANDING:
        obj->anim = ANIM_STAND;
        obj->nextState = STATE_SITTING;
        obj->ticksUntilNextState = 25;
        break;

    case STATE_SITTING:
        obj->anim = ANIM_SIT;
        obj->nextState = STATE_LAYING;
        obj->ticksUntilNextState = 50;
        break;

    case STATE_LAYING:
        obj->anim = ANIM_LAY;
        obj->nextState = STATE_SLEEPING;
        obj->ticksUntilNextState = 100;
        break;

    case STATE_SLEEPING:
        obj->anim = ANIM_SLEEP;
        obj->nextState = STATE_NONE;
        obj->ticksUntilNextState = 0;
        break;

    case STATE_WALKING_LEFT:
    case STATE_WALKING_RIGHT:
        obj->dx = (state == STATE_WALKING_LEFT ? -PLAYER_WALK_SPEED : PLAYER_WALK_SPEED);
        obj->anim = ANIM_WALK;
        obj->nextState = STATE_NONE;
        obj->ticksUntilNextState = 0;
        break;

    case STATE_EATING:
        obj->anim = ANIM_EAT;
        obj->nextState = obj->returnToState;
        obj->ticksUntilNextState = 12;
        break;

    case STATE_POOP_ATTEMPT:
        obj->anim = ANIM_POOP;
        obj->nextState = STATE_POOPING;
        obj->ticksUntilNextState = 12;
        break;

    case STATE_POOPING:
        obj->anim = ANIM_POOP;
        obj->nextState = STATE_POOP_FINISHING;
        obj->ticksUntilNextState = 1; // This must be 1, otherwise you'll drop more than one poop at a time.
        break;

    case STATE_POOP_FINISHING:
        obj->anim = ANIM_POOP;
        obj->nextState = obj->returnToState;
        obj->ticksUntilNextState = 4;
        break;

    case STATE_BARKING:
        obj->anim = ANIM_BARK;
        obj->nextState = obj->returnToState;
        obj->ticksUntilNextState = 8;
        break;
    }
}

void readButtons()
{

    // This function adds PRESS and RELEASE events to the queue.

    // cycle through each button...
    for (byte b = 0; b < BUTTONS; b++)
    {

        // Remember the state of this button from the last pass.
        button[b].was = button[b].is;
        button[b].is = digitalRead(button[b].pin) == LOW;

        // Is the button down?
        if (button[b].is)
        {

            if (!(button[b].was))
            { // Just pressed

                switch (b)
                {

                case BUTTON_LEFT:
                    changeState(&player, STATE_WALKING_LEFT);
                    break;

                case BUTTON_RIGHT:
                    changeState(&player, STATE_WALKING_RIGHT);
                    break;

                case BUTTON_BARK:
                    if ((player.state >= STATE_STANDING) && (player.state <= STATE_SLEEPING))
                        player.returnToState = STATE_STANDING;
                    else
                        player.returnToState = player.state;
                    changeState(&player, STATE_BARKING);
                    break;

                case BUTTON_POOP:
                    if (treatsInBelly)
                    {
                        if ((player.state >= STATE_STANDING) && (player.state <= STATE_SLEEPING))
                            player.returnToState = STATE_STANDING;
                        else
                            player.returnToState = player.state;
                        changeState(&player, STATE_POOP_ATTEMPT);
                    }
                    break;
                }
            }
        }
        else
        { // Not down?

            if (button[b].was)
            { // Just released

                switch (b)
                {
                case BUTTON_LEFT:
                case BUTTON_RIGHT:
                    changeState(&player, STATE_STANDING);
                    break;
                }
            }
        }
    }
}

void collisionDetection()
{

    // This function assumes that a treat can't be in the same position as another.

    for (byte t = 0; t < treats; t++)
    {
        if (abs(player.x - treat[t].x) <= PLAYER_COLLISION_RANGE)
        {

            treatsInBelly++;

            // move the last treat to the collided one, and shorten the list.
            treat[t] = treat[treats - 1];
            treats--;

            player.returnToState = player.state;
            changeState(&player, STATE_EATING);
            break;
        }
    }
}

void updateGame()
{

    switch (player.state)
    {

    case STATE_WALKING_LEFT:
    case STATE_WALKING_RIGHT:

        // Move the player, and contrain her to the level width.
        player.x += player.dx;
        player.x = constrain(player.x, PLAYER_WIDTH / 2, LEVEL_WIDTH * TILE_WIDTH - PLAYER_WIDTH / 2);

        // At the moment, only the player moves, so we only need to do collision detection here.
        // If I add enemies, I'll need to move collision detection outside this switch/case statement.
        collisionDetection();
        break;

    case STATE_POOPING:
        // If we've got some poops left, and we're a little way through the poop animation, drop one.
        if (treatsInBelly)
        {
            treatsInBelly--;
            char dir = player.dx < 0 ? -1 : 1;
            poop[poops].x = player.x - (PLAYER_X_OFFSET * dir) + (POOP_X_OFFSET * dir);
            poops++;
        }
        break;
    }

    // Are we counting down to a next state?
    if (player.ticksUntilNextState > 0)
    {
        player.ticksUntilNextState--;
        if (player.ticksUntilNextState == 0)
            changeState(&player, player.nextState);
    }

    // Animate the player.
    player.tick++;
    if (player.tick >= anims[player.anim].frameDelay)
    {
        player.tick = 0;
        player.frame++;
        if (player.frame >= anims[player.anim].length)
            player.frame = 0;
    }

    // Animate the flowers.
    flowerTick++;
    if (flowerTick >= FLOWER_TICKS)
    {
        flowerTick = 0;
        flowerFrame++;
        if (flowerFrame >= FLOWER_FRAMES)
            flowerFrame = 0;
    }
}

void render()
{

    // Draws the sprites to a hidden window, and then shifts into view when done.
    // We need to do it this way, otherwise there's a lot of flickering caused by the .cls() and time it takes to blit
    // everything.

    // Clear everything, ready for blitting.
    vfd.cls();

    // Center the display X on the player, constrained by the level size.
    int dispX = constrain((int)player.x - (int)vfd.displayWidth / 2, 0, (int)LEVEL_WIDTH * (int)TILE_WIDTH - (int)vfd.displayWidth);

    // draw the background.
    // We need to know haw much of the first tile is over the left edge.
    byte tileOffset = dispX % TILE_WIDTH;

    // We then go through all the tiles on the screen (+1 if the first tile is half over the left edge).
    for (byte x = 0; x < vfd.displayWidth / TILE_WIDTH + (tileOffset > 0 ? 1 : 0); x++)
    {

        byte t = mapData[x + dispX / TILE_WIDTH]; // Get the tile from the map.
        if (t == TILE_FLOWER)
        {
            t += flowerFrame; // If it's a flower, animate it (flowerFrame is set in updateGame()).
        }

        // If it's not blank, draw the tile.
        if (t)
        {
            vfd.blit(
                tiles[t - 1],
                x * TILE_WIDTH - tileOffset,
                0,
                false);
        }
    }

    // Blit the treats
    // There the .blit() function already checks if a sprite's X,Y position is on the screen, so I don't check here.
    for (byte t = 0; t < treats; t++)
    {
        vfd.blit(
            sprites[TREAT_SPRITE],
            treat[t].x - dispX,
            TREAT_Y_POSITION,
            false);
    }

    // Blit the poops
    for (byte p = 0; p < poops; p++)
    {
        vfd.blit(
            sprites[POOP_SPRITE],
            poop[p].x - dispX,
            POOP_Y_POSITION,
            false);
    }

    // Blit the player ("Sunny" the pooch)
    vfd.blit(
        sprites[anims[player.anim].frames[player.frame]],
        player.x - dispX,
        PLAYER_Y_POSITION,
        player.dx < 0);

    /*
    char buffer[17];
    sprintf(buffer, "%u", debug);
    vfd.printXY(0, 0, buffer);
    */

    // Show the changes by shifting the hidden display into view.
    vfd.scroll(vfd.displayWidth * vfd.displayHeight >> 3, 1, 0);
}

void initLevel()
{

    // Reset the player, treats, and poops.
    player.x = 0;
    player.dx = 0;
    changeState(&player, STATE_STANDING);

    treats = 0;
    treatsInBelly = 0;

    poops = 0;

    // Copy the level data to the map, picking out the player and treats as we encounter them.
    for (byte x = 0; x < LEVEL_WIDTH; x++)
    {
        byte t = level[x];
        switch (t)
        {

        case TILE_PLAYER:
            player.x = x * TILE_WIDTH + TILE_WIDTH / 2;
            t = 0;
            break;

        case TILE_TREAT:
            if (treats < MAX_TREATS)
            {
                treat[treats].x = x * TILE_WIDTH + TILE_WIDTH / 2;
                treats++;
            }
            t = 0;
            break;
        }
        mapData[x] = t;
    }
}

# gu7000

Noritake Itron GU-7000 VFD Arduino library.

See Noritake Itron's [Design Resources](https://www.noritake-elec.com/support/design-resources?series=7000) and
[Arduino code library quick-start guide](https://www.noritake-elec.com/support/design-resources/support-guide/arduino-code-library-quick-start-guide)
for their official library.

This library is an alternative that supports drawing sprites at any (x,y) position with clipping, even on the earlier
"A" revision VFDs with firmware F100.

See http://retrojdm.com/Arduino_GU144X16D-7053B.asp for more info.

## Demo YouTube videos:
- [Arduino VFD Dog Simulator](https://www.youtube.com/watch?v=n0dmCTFcPYk)
- [Dog Platform Game on a VFD](https://www.youtube.com/watch?v=-vDEB7bk588)
#pragma once

// The UART HAL is just a wrapper for a serial port and the digitalRead/digitalWrite functions.
//
// This seems unneccessary at first, but this extra layer is in place to allow custom HALs for cases where you want to
// use direct port manipulation instead of the inefficient digitalRead/digitalWrite functions, or I2C or SPI instead of
// Asyncronous Serial mode (UART) to write data.
//
// The compiler should just "inline" the functions anyway, so there's no hard done.

#include <Arduino.h>
#include "gu7000HalBase.h"

class gu7000UartHal : public gu7000HalBase
{
public:
  gu7000UartHal(Stream *virtualPort, uint8_t busyPin, uint8_t resetPin);

  void write(uint8_t data);
  void writeResetPin(uint8_t value);
  uint8_t readBusyPin();

private:
  Stream *_virtualPort;
  uint8_t _busyPin;
  uint8_t _resetPin;
};
#include "gu7000UartHal.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class constructor

gu7000UartHal::gu7000UartHal(Stream *virtualPort, uint8_t busyPin, uint8_t resetPin)
{
  _virtualPort = virtualPort;
  _busyPin = busyPin;
  _resetPin = resetPin;

  pinMode(busyPin, INPUT);
  pinMode(resetPin, OUTPUT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Public methods

void gu7000UartHal::write(uint8_t data)
{
  _virtualPort->write(data);
}

void gu7000UartHal::writeResetPin(uint8_t value)
{
  digitalWrite(_resetPin, value);
}

uint8_t gu7000UartHal::readBusyPin()
{
  return digitalRead(_busyPin);
}
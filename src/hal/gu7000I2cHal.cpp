#include "gu7000I2cHal.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class constructor

gu7000I2cHal::gu7000I2cHal(uint8_t address, uint8_t busyPin, uint8_t resetPin)
{
  _address = address;
  _busyPin = busyPin;
  _resetPin = resetPin;

  pinMode(busyPin, INPUT);
  pinMode(resetPin, OUTPUT);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Public methods

void gu7000I2cHal::write(uint8_t data)
{
  Wire.beginTransmission(_address);
  Wire.write(data);
  Wire.endTransmission();
}

void gu7000I2cHal::writeResetPin(uint8_t value)
{
  digitalWrite(_resetPin, value);
}

uint8_t gu7000I2cHal::readBusyPin()
{
  return digitalRead(_busyPin);
}
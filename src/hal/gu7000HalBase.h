#pragma once

// Hardware Abstraction Layer abstract class ("Interface").
class gu7000HalBase
{
public:
  virtual void write(uint8_t data) = 0;
  virtual void writeResetPin(uint8_t value) = 0;
  virtual uint8_t readBusyPin() = 0;
};
#pragma once

// Noritake Itron GU-7000 series Arduino library
//
// By Andrew Wyatt
// retrojdm.com
//
// See https://www.noritake-elec.com/products/vfd-display-module/dot-matrix-graphic-display/gu-7000-series
//
// This library currently has implementations of the Hardware Abstraction Layer (HAL) to support both asynchronous
// serial (UART) mode and I2C mode. You can create a custom HAL for SPI or synchronous serial mode. Take a look at the
// gu7000UartHal and gu7000I2cHal classes for an examples.
//
// Note: See the above link to ensure your model actually supports I2C mode etc.
//
// The following table is from the GU112X16G-7003 datasheet, but the default is the same for the GU144X16D-7053B.
// You'll have to check the datasheet of your particular display.
//
// | J2    | J3    | Interface type                |
// | :---- | :---- | :---------------------------- |
// | OPEN  | OPEN  | Asynchronous serial interface | <-- Default
// | SHORT | OPEN  | Synchronous serial interface  |
// | OPEN  | SHORT | I2C interface                 |
// | SHORT | SHORT | SPI                           |
//
// The default baud rate is 38400. If you want to use a different baud rate, you'll have to solder some jumpers.
//
// | J0    | J1    | Baud rate  |
// | :---- | :---- | :--------- |
// | OPEN  | OPEN  | 38,400bps  | <-- Default
// | SHORT | OPEN  | 19,200bps  |
// | OPEN  | SHORT | 9,600bps   |
// | SHORT | SHORT | 115,200bps |
//
// The VFD's pinout for asynchronous serial (UART) is:
//
// | VFD Pin | Signal Name | Function     |
// | :-----: | :---------- | :----------- |
// |    1    | VCC         | Power Supply |
// |    2    | SIN         | Data Receive |
// |    3    | GND         | Ground       |
// |    4    | SBUSY       | Display busy |
// |    5    | NC          | -            |
// |    6    | /RESET      | Reset        |
// |    7    | NC          | -            |

#include "Arduino.h"
#include "hal/gu7000HalBase.h"

// function signature for a method to report error codes.
typedef void (*displayErrorCallback)(uint8_t);

typedef struct sprite
{
    uint16_t width; // Max 512px.
    uint8_t height; // Must me a multiple of 8. Max 32px.
    uint16_t xOffset;
    uint8_t yOffset;
    const uint8_t *data;
};

class gu7000
{

public:
    uint16_t displayWidth;  // dots
    uint16_t displayHeight; // dots
    uint16_t memoryWidth;   // dots
    uint16_t memoryHeight;  // dots

    // Constructor.
    //
    // displayWidth and displayHeight should be in the datasheet under the Display Specification section as
    // Grahpic display - Number of dots.
    // Eg:  GU112X16G-7003  = 112 x 16 dots
    //      GU144X16D-7053B = 144 x 16 dots
    //
    // memoryWidth and memoryHeight should be in the same section as Display Memory - Size.
    // Eg: The GU112X16G-7003 and GU144X16D-7053B both have a display memory size of 512 x 16 dots.
    //
    // Note: this library only uses the memory width and height for clipping sprites.
    //
    // The HAL is a Hardware Abstrction Layer to allow for custom data transfer and pin read/write methods.
    // The default `gu7000UartHal` class uses UART. `gu7000I2cHal` is provided for I2C if your display supports it.
    // You could create your own for SPI etc.
    gu7000(
        uint16_t displayWidth,
        uint16_t displayHeight,
        uint16_t memoryWidth,
        uint16_t memoryHeight,
        gu7000HalBase *pHal);

    // A second signature allows you to specify an error callback function for reporting error codes.
    // This library only throws error code "2" - which means it timed out after 1 second waiting for the SBusy pin to go
    // LOW.
    // This probably means the display RESET or SBusy pins aren't wired correctly (or configured to the correct pin
    // numbers).
    gu7000(
        uint16_t displayWidth,
        uint16_t displayHeight,
        uint16_t memoryWidth,
        uint16_t memoryHeight,
        gu7000HalBase *pHal,
        displayErrorCallback callback);

    // Resets the display.
    void reset();

    // Settings return to default values.
    // Jumper settings are not re-loaded.
    // Contents of receive buffer remains in memory.
    void init();

    // Clears the current window.
    void cls();

    // Prints text at the current cursor position.
    //
    // Special characters:
    //
    // 0x08        Backspace           Cursor moves left by one character
    // 0x09        Tab                 Cursor moves right by one character
    // 0x0A        Line Feed           Cursor moves to next lower line
    // 0x0B        Home Position       Cursor moves to home position (top left)
    // 0x0D     Carriage Return     Cursor moves to the left end of current line
    void print(char *text);

    // Sets the current cursor position.
    void setCursor(uint16_t x, uint16_t row);

    // Shows or hides the cursor.
    void showCursor(boolean bShow);

    // Prints custom characters or the originals. Only effects subsequent prints.
    void showCustomChars(boolean bShow);

    // Defines custom character bit images.
    // You can define multiple characters, but make sure to provide enough data.
    // Each uint8_t of data is effectively a single column in a character.
    //
    // charWidth must be either 5 or 7.
    void downloadCustomChars(uint8_t charWidth, char charCodeFrom, char charCodeTo, uint8_t *data);

    void deleteCustomChar(char charCode);

    // Sets which font set to use.
    //
    // fontSet:
    // 0x00    America
    // 0x01    France
    // 0x02    Germany
    // 0x03    England
    // 0x04    Denmark 1
    // 0x05    Sweden
    // 0x06    Italy
    // 0x07    Spain 1
    // 0x08    Japan
    // 0x09    Norway
    // 0x0A    Denmark 2
    // 0x0B    Spain 2
    // 0x0C    Latin America
    // 0x0D    Korea
    void internationalFontSet(uint8_t fontSet);

    // Sets which character table to use.
    //
    // tableType:
    // 0x00     PC437 (USA - Euro std)
    // 0x01     Katakana - Japanese
    // 0x02     PC850 (Multilingual)
    // 0x03     PC860 (Portugese)
    // 0x04     PC863 (Canadian-French)
    // 0x05     PC865 (Nordic)
    // 0x10     WPC1252
    // 0x11     PC866 (Cyrillic #2)
    // 0x12     PC852 (Latin 2)
    // 0x13     PC858
    void charTableType(uint8_t tableType);

    // Sets the display mode.
    // mode:
    // 0x01     Overwrite
    // 0x02     Vertical-scroll
    // 0x03     Horizontal-scroll
    void displayMode(uint8_t mode);

    // Sets the horizontal scroll speed.
    // n:
    // 0x00         Instantaneous (default)
    // 0x01         T ms / 2 dots
    // 0x02..0x1f   (n-1)*T ms/dot
    void horizScrollSpeed(uint8_t n);

    // Toggles reverse print mode.
    void reverse(boolean bOffOn);

    // Sets the render mode for both text and images.
    //
    // mode:
    // 0x00     Normal display write (not mixture display)
    // 0x01     OR display write
    // 0x02     AND display write
    // 0x03     EX-OR display write
    void writeMixDisplayMode(uint8_t mode);

    // Sets the display brightness.
    //
    // level:   0x00..0x08
    void brightness(uint8_t level);

    // Causes the VFD to wait.
    //
    // Wait time = t x approximately 0.5s
    void wait(uint8_t time);

    // Shifts the display data in memory.
    //
    // Can be used for scrolling or double-buffering (i.e.: reduce flickering by drawing off-screen then shifting into
    // view when ready).
    //
    // Because of how display memory is organised, you'll probably need to shift the data more than one place to scroll
    // by one pixel. Eg: For a 112x16px display (two-rows), shift by...:
    //
    // - 2 places to scroll left by 1 pixel.
    // - 224 places to scroll left by an entire screen (112px).
    //
    // Note: You'll need to call .writeScreenMode(1); before drawing in the hidden area.
    void scroll(uint16_t width, uint16_t cycles, uint8_t speed);

    // Blinks the entire display.
    //
    // pattern:
    // 0x00    Normal display.
    // 0x01    Blink display (alternately Normal and Blank display).
    // 0x02    Blink display (alternately Normal and Reverse display).
    void blink(uint8_t pattern, uint8_t normalTime, uint8_t blankOrReverseTime, uint8_t cycles);

    // Turns the display on/off to save the display when idle.
    //
    // mode:
    // 0x00     Display power OFF (Power save mode)
    // 0x01     Display power ON
    // 0x02     All dot OFF
    // 0x03     All dot ON
    // 0x04     Repeat blink display with normal and Reverse display (Normal: 2s, Reverse: 2s)
    void screenSaver(uint8_t mode);

    // Prints text at any (x,y) position (only available on "B" revision VFDs).
    //
    // The special characters 0x10 and 0x11 are used to turn Reverse OFF and ON, respectively.
    //
    // Use x = 0xffff to write text from the previous cursor position.
    void printXY(uint16_t x, uint16_t line, char *text);

    // Sets the font width mode.
    //
    // mode:
    // 0x00    Fixed character width 1 (1 dot space on right side)
    // 0x01    Fixed character width 2 (1 dot space on right side and left side)
    // 0x02    Proportional character width 1 (1 dot space on right side)
    // 0x03    Proportional character width 2 (1 dot space on right side and left side)
    void fontWidthMode(uint8_t mode);

    // Sets the font size.
    //
    // x = 1..4
    // y = 1..2
    void fontMagnification(uint8_t x, uint8_t y);

    // Selects the window to print/draw to.
    //
    // window:
    // 0x00         Base
    // 0x01..0x04   Window
    void windowSelect(uint8_t window);

    // Defines a window anywhere in display memory.
    void windowDefine(uint8_t window, uint16_t x, uint16_t row, uint16_t width, uint16_t rows);

    // Cancels the window definition.
    void windowCancel(uint8_t window);

    // Sets the screen mode when using the "base" window.
    //
    // mode:
    // 0x00    Display screen mode
    // 0x01    All screen mode
    void writeScreenMode(uint8_t mode);

    // Draws a sprite with the top-left corner at the current cursor position.
    //
    // Effectively calls the "real-time bit image display" command on the VFD directly.
    //
    // You can create bitmap data using either:
    //    http://www.noritake-elec.com/codeLibraries/gu7000/gu7000_BitmapImageTool.html
    //    http://en.radzio.dxp.pl/bitmap_converter/
    //
    void blit(sprite spr);

    // Draws a sprite with it's "handle" (x,y offset) at the given (x,y) position.
    //
    // This should work on all GU-7000 series VFDs (not just the later "B" generation ones) because it doesn't rely on
    // the later "dot unit" bit image commands. Instead, it prepares image data for use with the simpler row-based
    // "real-time bit image display" command.
    //
    // This function overcomes another limitation of the VFD: Even if we used the "dot unit" bit image commands on the
    // later "B" revision VFDs, we could still only take X and Y co-ords as unsigned ints (uint16_ts), which would mean we
    // couldn't blit to negative X and Y positions (i.e.: half off-screen).
    //
    // This function takes X and Y co-ords as ints. It then sends a clipped and shifted version of the sprite to the
    // VFD, with the width and height adjusted accordingly.
    void blit(sprite spr, int x, int y, boolean flipX);

private:
    // Remember the window's widths and heights for clipping.
    // There are four definable windows, plus the "base" window #0.
    uint8_t currentWindow;
    int winWidth[4];
    int winHeight[4];

    // Pointer to a Hardware Abstraction Layer object.
    gu7000HalBase *_pHal;

    // Callback function for reporting error codes.
    displayErrorCallback _callback;

    void waitWhileBusy();
    void write(uint8_t data);
};
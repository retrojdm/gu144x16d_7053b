#include "gu7000.h"

// Note: References and page numbers are for the GU144X16D-7053B datasheet.

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constructors

gu7000::gu7000(
    uint16_t width,
    uint16_t height,
    uint16_t memWidth,
    uint16_t memHeight,
    gu7000HalBase *pHal)
{
    displayWidth = width;
    displayHeight = height;
    memoryWidth = memWidth;
    memoryHeight = memHeight;

    // We remember the width and height of the windows for clipping sprites.
    currentWindow = 0;
    winWidth[0] = displayWidth;
    winHeight[0] = displayHeight;

    _pHal = pHal;
    _callback = nullptr; // Oh oh. If we have a timeout error, no-one will know!

    _pHal->writeResetPin(HIGH);
}

gu7000::gu7000(
    uint16_t width,
    uint16_t height,
    uint16_t memWidth,
    uint16_t memHeight,
    gu7000HalBase *pHal,
    displayErrorCallback callback)
{
    displayWidth = width;
    displayHeight = height;
    memoryWidth = memWidth;
    memoryHeight = memHeight;

    // We remember the width and height of the windows for clipping sprites.
    currentWindow = 0;
    winWidth[0] = displayWidth;
    winHeight[0] = displayHeight;

    _pHal = pHal;
    _callback = callback;

    _pHal->writeResetPin(HIGH);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Public functions

void gu7000::reset()
{
    // 5.3 Reset timing (page 7).
    // Send the Reset pulse.
    _pHal->writeResetPin(LOW);
    delay(10);
    _pHal->writeResetPin(HIGH);

    waitWhileBusy();

    // Data should be valid 1.5 microseconds after SBUSY goes LOW. So 1 millisecond is overkill.
    delay(1);
}

void gu7000::init()
{
    // 7.1.10 ESC @ (Initialize Display) (page 15).
    write(0x1B);
    write(0x40);
    delay(10);
}

void gu7000::cls()
{
    // 7.1.8 CLS (Display Clear) (page 15)
    write(0x0C);
}

void gu7000::print(char *text)
{
    // 7.1.1 Character Display (page 12).
    for (uint16_t c = 0; c < strlen(text); c++)
    {
        write(text[c]);
    }
}

void gu7000::setCursor(uint16_t x, uint16_t row)
{
    // 7.1.7 US $ xL xH yL yH (Cursor Set) (page 15)
    write(0x1F);
    write(0x24);
    write(x & 0xff);   // xL
    write(x >> 8);     // xH
    write(row & 0xff); // yL
    write(row >> 8);   // yH
}

void gu7000::showCursor(boolean bShow)
{
    // 7.1.9 US C n (Cursor display) (page 15)
    write(0x1F);
    write(0x43);
    write(bShow ? 0x01 : 0x00);
}

void gu7000::showCustomChars(boolean bShow)
{
    // 7.1.11 ESC % n (Download character ON/OFF) (page 16)
    write(0x1B);
    write(0x25);
    write(bShow ? 0x01 : 0x00);
}

void gu7000::downloadCustomChars(uint8_t charWidth, char charCodeFrom, char charCodeTo, uint8_t *data)
{
    // 7.1.12 ESC & a c1 c2 [x1 d1..d(a*x1)]..9xk d1..d(a*xk)] (Download character definition) (page 16)
    write(0x1B);
    write(0x26);
    write(0x01);
    write(charCodeFrom);
    write(charCodeTo);

    uint8_t chars = charCodeTo - charCodeFrom + 1;

    // Define the character data (character by character, column by column).
    for (char c = 0; c < chars; c++)
    {
        write(charWidth);
        for (uint8_t x = 0; x < charWidth; x++)
        {
            write(data[c * charWidth + x]);
        }
    }
}

void gu7000::deleteCustomChar(char charCode)
{
    // 7.1.13 ESC ? a c (Download character delete) (page 17)
    write(0x1B);
    write(0x3F);
    write(0x01);
    write(charCode);
}

void gu7000::internationalFontSet(uint8_t fontSet)
{
    // 7.1.14 ESC R n (International font set) (page 17)
    write(0x1B);
    write(0x52);
    write(fontSet);
}

void gu7000::charTableType(uint8_t tableType)
{

    // 7.1.15 ESC t n (Character table type) (page 17)
    write(0x1B);
    write(0x74);
    write(tableType);
}

void gu7000::displayMode(uint8_t mode)
{
    // 7.1.16 US MD3 (Over-write mode)          (page 17)
    // 7.1.17 US MD3 (Vertical scroll mode)     (page 17)
    // 7.1.18 US MD3 (Horizontal scroll mode)   (page 18)
    write(0x1F);
    write(mode);
}

void gu7000::horizScrollSpeed(uint8_t n)
{
    // 7.1.19 US s n (Horizontal scroll speed) (page 18)
    write(0x1F);
    write(0x73);
    write(n);
}

void gu7000::reverse(boolean bReverse)
{
    // 7.1.20 US r n (Reverse display) (page 18)
    write(0x1F);
    write(0x72);
    write(bReverse ? 0x01 : 0x00);
}

void gu7000::writeMixDisplayMode(uint8_t mode)
{
    // 7.1.21 US w n (Write mixture display mode) (page 19)
    write(0x1F);
    write(0x77);
    write(mode);
}

void gu7000::brightness(uint8_t level)
{
    // 7.1.22 US X n (Brightness level setting) (page 19)
    write(0x1F);
    write(0x58);
    write(level);
}

void gu7000::wait(uint8_t time)
{
    // 7.1.24 <Function 01h> US ( a 01h t (Wait) (page 19)
    write(0x1F);
    write(0x28);
    write(0x61);
    write(0x01);
    write(time);
}

void gu7000::scroll(uint16_t width, uint16_t cycles, uint8_t speed)
{
    // 7.1.25 <Function 10h> US ( a 10h wL wH cL cH s (Scroll display action) (page 20)
    write(0x1F);
    write(0x28);
    write(0x61);
    write(0x10);
    write(width & 0xff);  // wL
    write(width >> 8);    // wH
    write(cycles & 0xff); // cL
    write(cycles >> 8);   // cH
    write(speed);         // s
}

void gu7000::blink(uint8_t pattern, uint8_t normalTime, uint8_t blankOrReverseTime, uint8_t cycles)
{
    // 7.1.26 <Function 11h> US ( a 11h p t1 t2 c (Blink) (page 21)
    write(0x1F);
    write(0x28);
    write(0x61);
    write(0x11);
    write(pattern);
    write(normalTime);
    write(blankOrReverseTime);
    write(cycles);
}

void gu7000::screenSaver(uint8_t mode)
{
    // 7.1.27 <Function 40h> US ( a 40h p (Screen saver) (page 21)
    write(0x1F);
    write(0x28);
    write(0x61);
    write(0x40);
    write(mode);
}

void gu7000::printXY(uint16_t x, uint16_t line, char *text)
{
    uint8_t len = strlen(text);

    // 7.1.33 <Function 30h> US ( d 30h xPL xPH yPL yPH m bLen d(1)...d(bLen) (Dot unit character display) (page 26)
    write(0x1F);
    write(0x28);
    write(0x64);
    write(0x30);
    write(x & 0xff);    // xPL
    write(x >> 8);      // xPH
    write(line & 0xff); // yPL
    write(line >> 8);   // yPH
    write((byte)0x00);  // m
    write(len);         // bLen

    for (uint8_t c = 0; c < len; c++)
    {
        write(text[c]);
    }
}

void gu7000::fontWidthMode(uint8_t mode)
{

    // 7.1.35 <Function 03h> US ( g 03h w (Font Width) (page 27)
    write(0x1F);
    write(0x28);
    write(0x67);
    write(0x03);
    write(mode);
}

void gu7000::fontMagnification(uint8_t x, uint8_t y)
{
    // 7.1.36 <Function 40h> US ( g 40h x y (Font Magnification) (page 27)
    write(0x1F);
    write(0x28);
    write(0x67);
    write(0x40);
    write(x);
    write(y);
}

void gu7000::windowSelect(uint8_t window)
{
    // 7.1.41 WINx (Window select shortcut) (page 30)
    write(0x10 | window);
    currentWindow = window;
}

void gu7000::windowDefine(uint8_t window, uint16_t x, uint16_t row, uint16_t width, uint16_t rows)
{
    // 7.1.39 <Function 02h> US ( w 02h a b [xPL xPH yPL yPH xSL xSH ySL ySH] (User Window define / cancel) (page 29)
    write(0x1F);
    write(0x28);
    write(0x77);
    write(0x02);
    write(window);       // a
    write(0x01);         // b (0x01 = define)
    write(x & 0xff);     // xPL
    write(x >> 8);       // xPH
    write(row & 0xff);   // yPL
    write(row >> 8);     // yPH
    write(width & 0xff); // xSL
    write(width >> 8);   // xSH
    write(rows & 0xff);  // ySL
    write(rows >> 8);    // ySH

    // We remember the width / height of the window for clipping sprites.
    winWidth[window] = width;
    winHeight[window] = rows * 8;
}

void gu7000::windowCancel(uint8_t window)
{
    // 7.1.39 <Function 02h> US ( w 02h a b [xPL xPH yPL yPH xSL xSH ySL ySH] (User Window define / cancel) (page 29)
    write(0x1F);
    write(0x28);
    write(0x77);
    write(0x02);
    write(window);     // a
    write((byte)0x00); // b (0x00 = cancel)

    winWidth[window] = 0;
    winHeight[window] = 0;
}

void gu7000::writeScreenMode(uint8_t mode)
{
    // 7.1.40 <Function 10h> US ( w 10h a (Write screen mode select) (page 30)
    // See 6.4 Write screen mode (page 10)
    write(0x1F);
    write(0x28);
    write(0x77);
    write(0x10);
    write(mode);

    // In "All Screen" mode, we can write to the entire memory width.
    winWidth[0] = (mode == 1 ? memoryWidth : displayWidth);
}

void gu7000::blit(sprite spr)
{
    uint16_t rows = spr.height >> 3;

    // 7.1.32 <Function 21h> US ( d 21h xPL xPH yPL yPH xL xH yL yH g d(1)...d(k)
    // (Dot unit real-time bit image display) (page 24)
    write(0x1F);
    write(0x28);
    write(0x66);
    write(0x11);
    write(spr.width & 0xff); // xL
    write(spr.width >> 8);   // xH
    write(rows & 0xff);      // yL
    write(rows >> 8);        // yH
    write(0x01);             // g

    const uint8_t *pData = spr.data;
    uint16_t k = spr.width * rows;
    for (uint16_t i = 0; i < k; i++)
    {
        write(*(pData++));
    }
}

void gu7000::blit(sprite spr, int x, int y, boolean flipX)
{
    // Get the sprite's bounding box (in pixels).
    int x1 = x - spr.xOffset;
    int y1 = y - spr.yOffset;
    int x2 = x1 + spr.width - 1;
    int y2 = y1 + spr.height - 1;

    // Is the sprite off the screen?
    // (these are integers, so that we don't need to cast for comparison with our unsigned bounds).
    int maxWidth = winWidth[currentWindow];
    int maxHeight = winHeight[currentWindow];
    if ((x2 < 0) || (x1 >= maxWidth) || (y2 < 0) || (y1 >= maxHeight))
    {
        return;
    }

    // Figure out the sprite's clipped dimensions.
    uint16_t clippedX1 = (x1 < 0 ? 0 : x1);
    uint8_t clippedY1 = (y1 < 0 ? 0 : y1);
    uint16_t clipLeft = (x1 < 0 ? abs(x1) : 0);
    uint8_t clipTop = (y1 < 0 ? abs(y1) : 0);
    uint16_t clippedWidth = constrain(spr.width - clipLeft, 0, maxWidth - clippedX1);
    uint8_t clippedHeight = constrain(spr.height - clipTop, 0, maxHeight - clippedY1);

    uint8_t clippedY2 = clippedY1 + clippedHeight - 1;
    uint8_t r1 = clippedY1 >> 3;
    uint8_t r2 = clippedY2 >> 3;
    uint8_t renderRows = r2 - r1 + 1;
    uint8_t dataRows = spr.height >> 3;
    uint8_t shiftOffset = y1 >= 0 ? y1 % 8 : 8 - (clipTop % 8);
    int8_t dataRowOffset = (clipTop >> 3) + (clipTop > 0 && shiftOffset > 0 ? 1 : 0);

    setCursor(clippedX1, r1);

    // 7.1.29 <Function 11h> US ( f 11h xL xH yL yH g d(1)...d(k) (Real-time bit image display) (page 24).
    write(0x1F);
    write(0x28);
    write(0x66);
    write(0x11);
    write(clippedWidth & 0xff); // xL
    write(clippedWidth >> 8);   // xH
    write(renderRows & 0xff);   // yL
    write(renderRows >> 8);     // yH
    write(0x01);                // g

    for (uint16_t i = 0; i < clippedWidth; i++)
    {
        uint16_t dataX = flipX ? spr.width - 1 - i - clipLeft : clipLeft + i;

        for (uint8_t row = 0; row < renderRows; row++)
        {
            // Get the previous row as the high byte, then shift down.
            int8_t r = dataRowOffset + row;
            const uint8_t *ptr = spr.data + (dataX * dataRows) + r;
            uint8_t dH = r - 1 >= 0 ? *(ptr - 1) : 0;
            uint8_t dL = r < dataRows ? *ptr : 0;
            uint16_t w = ((((uint16_t)dH) << 8) | dL) >> shiftOffset;
            uint8_t d = w & 0xff;

            write(d);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Private functions

void gu7000::waitWhileBusy()
{
    const uint32_t timeoutSeconds = 10;
    static uint32_t lastCheckedIfBusyMicros = 0;

    // The datasheet shows that it will be at least 20 microseconds after the VFD receives data before the SBusy signal
    // can be relied on. So we need to make sure it's been at least that long before checking the SBusy signal.
    //
    // The micros() counter will overflow every 70 minutes or so. Luckily, being an unsigned long the duration will be
    // calculated as a huge number and we'll still exit the loop.
    uint32_t now;
    do
    {
        now = micros();
    } while (now - lastCheckedIfBusyMicros < 20);

    uint32_t started = now;
    bool busy;
    do
    {
        busy = _pHal->readBusyPin() == HIGH;
        lastCheckedIfBusyMicros = micros();

        // 1,000,000 micros = 1 second timeout.
        if ((lastCheckedIfBusyMicros - started >= (1000000 * timeoutSeconds)) && (_callback != nullptr))
        {
            // I use error code "1" to mean "malloc failed" in other libraries (eg: AvagoDisplay).
            // We'll use error code "2" here to mean "timed out".
            _callback(2);
            return;
        }

    } while (busy);
}

void gu7000::write(uint8_t data)
{
    waitWhileBusy();
    _pHal->write(data);
}